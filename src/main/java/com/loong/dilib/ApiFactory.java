package com.loong.dilib;

import com.loong.dilib.core.ApiInterceptor;
import com.loong.dilib.core.ApiProxy;
import com.loong.dilib.exception.DIAnnotationException;

/**
 * Api实例化工具类工厂
 * 
 * @author 张成轩
 */
public class ApiFactory {

	private static ApiProxy proxy;

	/**
	 * @return 获取Api代理
	 */
	private static ApiProxy getProxy() {

		if (proxy == null) {
			// 创建Api实体处理
			ApiInterceptor interceptor = new ApiInterceptor();
			// 创建Api代理类
			proxy = new ApiProxy();
			proxy.setInterceptor(interceptor);
		}
		return proxy;
	}

	/**
	 * 实例化Api对象
	 * 
	 * @param className 实例化接口名
	 * @return 实例对象
	 * @throws ClassNotFoundException
	 * @throws DIAnnotationException
	 */
	public static Object createApi(String className) throws ClassNotFoundException, DIAnnotationException {

		return getProxy().createApi(className);
	}

	/**
	 * 实例化Api对象
	 * 
	 * @param targetClass 接口类型
	 * @return 实例对象
	 * @throws DIAnnotationException
	 */
	public static <T> T createApi(Class<T> targetClass) throws DIAnnotationException {

		return getProxy().createApi(targetClass);
	}
}
