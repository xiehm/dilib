package com.loong.dilib.core;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.loong.dilib.annotation.DICookie;
import com.loong.dilib.annotation.DIHeader;
import com.loong.dilib.annotation.DIJson;
import com.loong.dilib.annotation.DIN;
import com.loong.dilib.annotation.DIRequest;
import com.loong.dilib.annotation.DIResponse;
import com.loong.dilib.exception.DIConnectException;
import com.loong.dilib.exception.DIFormatException;

import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

/**
 * Api代理实体处理类
 */
public class ApiInterceptor implements MethodInterceptor {

	private static String chartset = "UTF-8";
	private static CloseableHttpClient httpClient = HttpClients.createDefault();
	private static Gson gson = new Gson();

	private static final String content = "Content-Type";
	private static final String content_default = "application/x-www-form-urlencoded";
	private static final String content_json = "application/json";

	@Override
	public Object intercept(Object object, Method method, Object[] params, MethodProxy methodProxy)
			throws DIConnectException, DIFormatException {

		// 获取方法请求注释
		DIRequest request = method.getAnnotation(DIRequest.class);

		// 获取类请求注释
		DIRequest apiRequest = null;
		for (Class<?> clas : object.getClass().getInterfaces())
			if ((apiRequest = clas.getAnnotation(DIRequest.class)) != null)
				break;

		// 获取请求基础信息
		StringBuilder url = getUrl(apiRequest, request);
		// 请求头
		Map<String, String> header = new LinkedHashMap<String, String>();
		// Cookie
		StringBuilder cookie = new StringBuilder();
		// 请求参数
		StringBuilder param = new StringBuilder();

		// 遍历参数
		Annotation[][] psAs = method.getParameterAnnotations();
		// 参数索引
		int pi = 0;
		for (Annotation[] pAs : psAs) {
			// 是否为请求头
			boolean isHeader = false;
			// 是否为请求参数
			boolean isJson = false;
			// 是否为Cookie请求
			boolean isCookie = false;
			// 简单参数注释
			DIN din = null;
			// 遍历注释判断
			for (Annotation pa : pAs)
				if (pa instanceof DIHeader)
					isHeader = true;
				else if (pa instanceof DIJson)
					isJson = true;
				else if (pa instanceof DICookie)
					isCookie = true;
				else if (pa instanceof DIN)
					din = (DIN) pa;
			// 处理方法参数
			if (isHeader)
				// 请求头
				if (din == null)
					pushHeader(params[pi], header);
				else
					pushHeader(din.value(), params[pi].toString(), header);
			else if (isCookie) {
				// Cookie
				if (din == null)
					pushCookie(params[pi], cookie);
				else
					pushCookie(din.value(), params[pi].toString(), cookie);
			} else if (isJson) {
				// 请求Json参数处理
				jsonParam(params[pi], param);
				// 添加头部信息
				pushHeader(content, content_json, header);
			} else {
				// 请求字符串参数处理
				if (din == null)
					pushParam(params[pi], param);
				else
					pushCookie(din.value(), params[pi].toString(), param);
				// 添加头部信息
				pushHeader(content, content_default, header);
			}
			pi++;
		}

		// 发送请求
		String html;
		if (request.method() == DIRequest.Method.POST)
			html = post(url, header, cookie, param);
		else
			html = get(url, header, cookie, param);

		// 处理响应信息
		DIResponse response = method.getAnnotation(DIResponse.class);
		if (response == null)
			// 获取类响应注释
			for (Class<?> clas : object.getClass().getInterfaces())
				response = clas.getAnnotation(DIResponse.class);
		if (html == null)
			return null;
		else if (method.getReturnType() == String.class)
			// 按字符串处理
			return html;
		else if (response == null || response.value() == DIResponse.Type.JSON)
			// Jsonp处理
			return fromJson(html, method.getGenericReturnType());
		else if (response.value() == DIResponse.Type.JSONP)
			// Json处理
			return fromJsonp(html, method.getGenericReturnType());
		return null;
	}

	/**
	 * 获取访问地址
	 * 
	 * @param apiRequest api请求注释
	 * @param request 方法请求注释
	 * @return url
	 */
	private StringBuilder getUrl(DIRequest apiRequest, DIRequest request) {

		if (apiRequest == null)
			// 没有方法请求注释，直接返回类注释的url
			return new StringBuilder(request.value());

		// 类注释访问地址
		String base = apiRequest.value();
		// 方法注释访问地址
		String servlet = request.value();

		// 拼装url
		boolean baseEnd = base.endsWith("/");
		boolean startUrl = servlet.startsWith("/");

		StringBuilder url = new StringBuilder(base);
		if (baseEnd && startUrl)
			url.append(servlet.substring(1));
		else if (!baseEnd && !startUrl)
			url.append("/").append(servlet);
		else
			url.append(servlet);
		return url;
	}

	/**
	 * 添加请求头
	 * 
	 * @param bean Bean对象
	 * @param header 请求头Map集合
	 */
	private void pushHeader(Object bean, Map<String, String> header) {

		Map<String, String> hs = convert(bean);
		header.putAll(hs);
	}

	/**
	 * 添加请求头
	 * 
	 * @param name 名称
	 * @param value 值
	 * @param header 请求头Map集合
	 */
	private void pushHeader(String name, String value, Map<String, String> header) {

		if (!header.containsKey(name))
			header.put(name, value);
	}

	/**
	 * 添加Cookie
	 * 
	 * @param bean Bean对象
	 * @param cookie 请求Cookie集合
	 */
	private void pushCookie(Object bean, StringBuilder cookie) {

		Map<String, String> cs = convert(bean);
		for (Entry<String, String> c : cs.entrySet())
			pushCookie(c.getKey(), c.getValue(), cookie);
	}

	/**
	 * 添加Cookie
	 * 
	 * @param name 名称
	 * @param value 值
	 * @param cookie 请求Cookie集合
	 */
	private void pushCookie(String name, String value, StringBuilder cookie) {

		if (cookie.length() != 0)
			cookie.append(";");
		cookie.append(name).append("=").append(value);
	}

	/**
	 * 添加字符串参数
	 * 
	 * @param bean Bean对象
	 * @param param 参数串
	 */
	private void pushParam(Object bean, StringBuilder param) {

		Map<String, String> ps = convert(bean);
		for (Entry<String, String> p : ps.entrySet())
			pushParam(p.getKey(), p.getValue(), param);
	}

	/**
	 * 添加字符串参数
	 * 
	 * @param name 名称
	 * @param value 值
	 * @param param 参数串
	 */
	private void pushParam(String name, String value, StringBuilder param) {

		if (param.length() != 0)
			param.append("&");
		param.append(name).append("=").append(value);
	}

	/**
	 * 添加Json参数
	 * 
	 * @param bean bean对象
	 * @param param 参数串
	 */
	private void jsonParam(Object bean, StringBuilder param) {

		if (param.length() == 0)
			param.append(convertJson(bean));
	}

	/**
	 * 反射转换Bean对象
	 * 
	 * @param bean Bean对象
	 * @return Map集合
	 */
	@SuppressWarnings("unchecked")
	private Map<String, String> convert(Object bean) {

		if (bean instanceof Map)
			return (Map<String, String>) bean;
		Map<String, String> map = new LinkedHashMap<String, String>();
		if (bean == null)
			return map;
		// 反射获取类
		Class<?> clas = bean.getClass();
		// 遍历父节点
		while (clas != null && !Object.class.equals(clas)) {
			// 获取变脸
			Field[] fields = clas.getDeclaredFields();
			for (Field f : fields) {
				f.setAccessible(true);
				Object value = null;
				try {
					// 获取变量值
					value = f.get(bean);
				} catch (IllegalAccessException e) {
					throw new RuntimeException(e);
				}
				// 添加到集合中
				map.put(f.getName(), value == null ? null : value.toString());
			}
			clas = clas.getSuperclass();
		}
		return map;
	}

	/**
	 * 获取Json字符串
	 * 
	 * @param bean Bean对象
	 * @return Json字符串
	 */
	private String convertJson(Object bean) {

		return gson.toJson(bean);
	}

	/**
	 * 获取对象
	 * 
	 * @param json Json字符串
	 * @param type 类型
	 * @return 对象
	 */
	@SuppressWarnings("unchecked")
	private <T> T fromJson(String json, Type type) {

		try {
			return (T) gson.fromJson(json, type);
		} catch (JsonSyntaxException e) {
			throw new DIFormatException("Response body format error: " + json, e);
		}
	}

	/**
	 * 获取对象
	 * 
	 * @param jsonp Jsonp字符串
	 * @param type 类型
	 * @return 对象
	 */
	@SuppressWarnings("unchecked")
	private <T> T fromJsonp(String jsonp, Type type) {

		String json = jsonp.substring(jsonp.indexOf("=") + 1, jsonp.lastIndexOf("}") + 1);
		return (T) gson.fromJson(json, type);
	}

	/**
	 * GET方式请求
	 * 
	 * @param url 访问地址
	 * @param header 请求头
	 * @param cookie 请求Cookie
	 * @param param 请求参数
	 * @return 响应体
	 * @throws DIConnectException
	 */
	private String get(StringBuilder url, Map<String, String> header, StringBuilder cookie, StringBuilder param)
			throws DIConnectException {

		String u = url.append("?").append(param).toString();
		HttpGet get = new HttpGet(u);
		// 添加请求头
		for (Entry<String, String> h : header.entrySet())
			get.addHeader(h.getKey(), h.getValue());
		// 添加Cookie
		if (cookie.length() != 0)
			get.addHeader("Cookie", cookie.toString());
		return getResponse(get);
	}

	/**
	 * POST方式请求
	 * 
	 * @param url 访问地址
	 * @param header 请求头
	 * @param cookie 请求Cookie
	 * @param param 请求参数
	 * @return 响应体
	 * @throws DIConnectException
	 */
	private String post(StringBuilder url, Map<String, String> header, StringBuilder cookie, StringBuilder param)
			throws DIConnectException {

		HttpPost post = new HttpPost(url.toString());
		StringEntity entity = new StringEntity(param.toString(), chartset);
		// 添加参数
		post.setEntity(entity);
		// 添加请求头
		for (Entry<String, String> h : header.entrySet())
			post.addHeader(h.getKey(), h.getValue());
		// 添加Cookie
		if (cookie.length() != 0)
			post.addHeader("Cookie", cookie.toString());
		return getResponse(post);
	}

	/**
	 * 请求
	 * 
	 * @param request 请求
	 * @return 响应
	 * @throws IOException
	 */
	private static String getResponse(HttpUriRequest request) throws DIConnectException {

		String httpStr = null;
		CloseableHttpResponse response = null;
		try {
			response = httpClient.execute(request);
			int code = response.getStatusLine().getStatusCode();
			if (code == 200) {
				// 请求成功
				HttpEntity httpEntity = response.getEntity();
				httpStr = EntityUtils.toString(httpEntity, chartset);
			} else
				throw new DIConnectException("Connection failed. error code:" + code);
		} catch (IOException e) {
			throw new DIConnectException("Connection failed", e);
		} finally {
			if (response != null)
				try {
					response.close();
				} catch (IOException e) {
				}
		}
		return httpStr;
	}
}
